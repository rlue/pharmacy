<?php include 'layout/header.php'; ?>

 <?php 

 


 if(!empty($_GET['id'])){
  $id =  $_GET['id']; 
    $sql = "SELECT * FROM `unit` WHERE unit_id = $id";
  $result = $conn->query($sql);
  $unit = mysqli_fetch_assoc($result);
 }
  
  if ( !empty($_POST)) {
        
        // keep track post values
        $name = $_POST['unit_name'];
        $id = $_POST['unit_id'];
         
        // validate input
        $valid = true;
        if (empty($name)) { 
            $valid = false;
        }
         
        // insert data
        if ($valid) {
            
            $sql = "UPDATE `unit` SET unit_name='$name' WHERE unit_id=$id";
            $res = mysqli_query($conn, $sql);
            
            if($res){
              exit(header('Location: unit_list.php'));
            }else{
              $fmsg = "Data not inserted, please try again later.";
            }
          
        }
    }

 ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
         <div class="page-title">
              <div class="title_left">
                <h3>Unit </h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
             <div class="x_panel">
                  <div class="x_title">
                    <h2>Unit Form <small>Unit Edit</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <form id="demo-form2" action="unit_edit.php" method="post" data-parsley-validate class="form-horizontal form-label-left">
                    <?php if(!empty($fmsg)){ echo $fmsg;} ?>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Name <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="hidden" name="unit_id" value="<?php if(!empty($id)){ echo $id; } ?>" >
                          <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12" name="unit_name" value="<?php if(!empty($unit['unit_name'])){ echo $unit['unit_name']; } ?>">
                        </div>
                      </div>
                      
                      
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          <a href="unit_list.php" class="btn btn-primary" >Cancel</a>
                          <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
            </div>

          </div>
          <br />

          </div>
        

    <?php include 'layout/footer.php'; ?>
  
