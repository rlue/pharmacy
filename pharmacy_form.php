
<?php include 'layout/header.php'; ?>
 <?php 
  if ( !empty($_POST)) {
         
        // keep track post values
        $name = $_POST['pharmacy_name'];
        $quantity = $_POST['pharmacy_quantity'];
        $date = $_POST['expired_date'];
        $category = $_POST['pharmacy_category'];
        $manufacture = $_POST['pharmacy_manufacture'];
        $unit = $_POST['pharmacy_unit'];
        $buy = $_POST['buy_price'];
        $sell = $_POST['sell_price'];
        // validate input
        $valid = true;
        if (empty($name)) {
            $valid = false;
        }
        if (empty($date)) {
            $valid = false;
        }
        if (empty($category)) {
            $valid = false;
        }
        if (empty($manufacture)) {
            $valid = false;
        }
        if (empty($unit)) {
            $valid = false;
        }
        if (empty($buy)) {
            $valid = false;
        }
        if (empty($sell)) {
            $valid = false;
        }
        if(empty($quantity)){
          $valid = false;
        }
        // insert data
        if ($valid) {
            
            $sql = "INSERT INTO pharmacy (pharmacy_name,expired_date,pharmacy_category,pharmacy_manufacture,pharmacy_unit,pharmacy_quantity,buy_price,sell_price) values('$name','$date','$category','$manufacture','$unit','$quantity','$buy','$sell')";
            $res = $conn->query($sql);
            
            if($res){
              exit(header('Location: pharmacy_list.php'));
            }else{
              $fmsg = "Data not inserted, please try again later.";
            }
          
        }
    }
    $sql_category = "SELECT * FROM category";
    $category = $conn->query($sql_category);

    $sql_unit = "SELECT * FROM unit";
    $unit = $conn->query($sql_unit);
 ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
         <div class="page-title">
              <div class="title_left">
                <h3>Pharmacy</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="alert alert-">
                    <?php if(!empty($fmsg)){
                      echo $fmsg;
                      }?>
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
             <div class="x_panel">
                  <div class="x_title">
                    <h2>Pharmacy Form <small>Pharmacy list</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <form id="demo-form2" action="pharmacy_form.php" method="post" data-parsley-validate class="form-horizontal form-label-left">

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Pharmacy Name <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="pharmacy_name" name="pharmacy_name" required="required" class="form-control col-md-7 col-xs-12">
                        </div>

                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Expired Date <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="datepicker" name="expired_date" required="required" class="form-control col-md-7 col-xs-12 datepicker">
                        </div>

                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Category<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select name="pharmacy_category" class="form-control" required="required">
                          <option value="">Select Category</option>
                          <?php while($cat = $category->fetch_assoc()) { ?>
                          <option value="<?php echo $cat['category_id']; ?>"><?php echo $cat['category_name']; ?></option>
                          <?php }?>
                          </select>
                        </div>

                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Manufacture<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="pharmacy_manufacture" name="pharmacy_manufacture" required="required" class="form-control col-md-7 col-xs-12">
                        </div>

                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Unit<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                         <select name="pharmacy_unit" class="form-control" required="required">
                          <option value="">Select Unit</option>
                          <?php while($u = $unit->fetch_assoc()) { ?>
                          <option value="<?php echo $u['unit_id']; ?>"><?php echo $u['unit_name']; ?></option>
                          <?php }?>
                          </select>
                        </div>

                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Quantity<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="pharmacy_quantity" name="pharmacy_quantity" required="required" class="form-control col-md-7 col-xs-12">
                        </div>

                      </div>
                     <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Buy Price<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="buy_price" name="buy_price" required="required" class="form-control col-md-7 col-xs-12">
                        </div>

                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Sell Price<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="sell_price" name="sell_price" required="required" class="form-control col-md-7 col-xs-12">
                        </div>

                      </div>
                     
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          <a href="pharmacy_list.php" class="btn btn-primary" >Cancel</a>
                          <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
            </div>

          </div>
          <br />

          </div>
        

    <?php include 'layout/footer.php'; ?>
