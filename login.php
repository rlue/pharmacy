<?php ob_start();
//Start session
  session_start();
?>
<?php include 'inc/connect.php'; ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" href="images/favicon.ico" type="image/ico" />

    <title>Admin Login | Pharmacy Store</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="css/nprogress.css" rel="stylesheet">
   
   
    <!-- bootstrap-daterangepicker -->
    <link href="css/daterangepicker.css" rel="stylesheet">

    <!-- Custom heme Style -->
    <link href="css/custom.min.css" rel="stylesheet">
  </head>

  <body class="login">
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>
      <?php
        if ( !empty($_POST)) {
        
        // keep track post values
        $name = mysqli_real_escape_string($conn,$_POST['user_name']);
        $password = sha1(mysqli_real_escape_string($conn,$_POST['password']));
        
         
        // validate input
        $valid = true;
        if (empty($name)) { 
            $valid = false;
        }
         
        if (empty($password)) {
            $valid = false;
        }
         
        // insert data
        if ($valid) {
            
            $sql = "SELECT * FROM users WHERE user_name='$name' AND password='$password'";
            $result = $conn->query($sql);
        $res = mysqli_fetch_assoc($result);
            
            if($res){
              $_SESSION["user_id"] = $res['user_id'];
              $_SESSION["user_name"] = $res['user_name'];
              exit(header('Location: index.php'));
            }else{
              $fmsg = "Please try again, Incorrect User name and password";
            }
          
        }
    }
      ?>
      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
            <form action="login.php" method="post">
              <h1>Login Form</h1>
              <?php if(!empty($fmsg)){?> 
                <div class="alert alert-danger"><?php echo $fmsg; ?></div>
              <?php } ?>
              <div>
                <input type="text" class="form-control" name="user_name" placeholder="Username" required="required" />
              </div>
              <div>
                <input type="password" name="password" class="form-control" placeholder="Password" required="required" />
              </div>
              <div>
                <button type="submit" class="btn btn-primary submit">Log in</button>
              </div>

              <div class="clearfix"></div>

              <div class="separator">

                <div class="clearfix"></div>
                <br />

                <div>
                  <h1>Pharmacy Store!</h1>
                  <p>©2018 All Rights Reserved. Pharmacy Store Develop by Hnin Ei Zin</p>
                </div>
              </div>
            </form>
          </section>
        </div>

        
      </div>
    </div>
  </body>
</html>
<?php ob_end_flush(); ?>