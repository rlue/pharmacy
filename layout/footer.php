</div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Pharmacy Store Develop By <a href="https://colorlib.com">Hnin Ei Zin</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>
    <!-- jQuery -->
    <script src="js/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="js/bootstrap.min.js"></script>
    <!-- FastClick -->
    
    <!-- NProgress -->
    <script src="js/nprogress.js"></script>
    <!-- Chart.js -->
 
    <!-- bootstrap-progressbar -->
    <script src="js/bootstrap-progressbar.min.js"></script>
   <script src="js/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="js/skycons.js"></script>
  
    <script src="js/bootstrap-datepicker.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="js/moment/moment.min.js"></script>
    <script src="js/datepicker/daterangepicker.js"></script>
    <script src="js/jquery.dataTables.min.js"></script>
    <script src="js/dataTables.bootstrap.min.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="js/custom.min.js"></script>
    <script src="js/select2.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function(){
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd'
        })
        $('#fromDate').datepicker({
            format: 'yyyy-mm-dd'
        })
        $('#toDate').datepicker({
            format: 'yyyy-mm-dd'
        })
        $('#category').select2();
        $('#pharmacy').select2();
        $('#supplier').select2();

        $('#category').on('change', function() {
          var phar = this.value;
          
          $.ajax({
           
            url: 'get_pharmacy.php',
            type: "GET",
            contentType: "application/json; charset=utf-8",
            data: { category_id: phar},
            error: function() {
                  $('#error').html('<p>An error has occurred</p>');
            },
            dataType: 'json',
            success: function(data) {
                $('#pharmacy').empty();
                $('#pharmacy').append('<option value="">Select Pharmacy</option>');
                var pharmacyList = '';
                $.each( data, function(index,value) {
                  
                   pharmacyList = '<option value="'+value['id']+'">'+value['name']+'</option>'
                  $('#pharmacy').append(pharmacyList);
                });
                
                console.log(pharmacyList);
            },
            
            });
        });
        $('#pharmacy').on('change', function() {
          var phar = this.value;
          $.ajax({
           
            url: 'get_price.php',
            type: "GET",
            contentType: "application/json; charset=utf-8",
            data: { pharmacy_id: phar},
            error: function() {
                  $('#error').html('<p>An error has occurred</p>');
            },
            dataType: 'json',
            success: function(data) {
                 
                 $('#price').val(data['sell_price']);
                  $('#pharmacy_qty').val(1);
            },
            
            });
        });
        $("#pharmacy_qty").on("change paste keyup", function() {
           var qty = $(this).val();
           var pharmacy = $('#pharmacy').val();
           var total ="";
           if(pharmacy){
                $.ajax({
               
                url: 'get_price.php',
                type: "GET",
                contentType: "application/json; charset=utf-8",
                data: { pharmacy_id: pharmacy},
                error: function() {
                      $('#error').html('<p>An error has occurred</p>');
                },
                dataType: 'json',
                success: function(data) {
                     
                     total = qty * data['sell_price'];
                     $('#price').val(total);
                },
                
                });
           }else{
             alert('Please select Pharmacy');
           }
           
           //var total = qty * price;
           
        });
    });
        
    </script>
    <?php ob_end_flush(); ?>
	</body>
</html>
