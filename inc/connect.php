<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "pharmacy";
// Create connection
$conn = new mysqli($servername, $username, $password,$dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

function checkUser()
{
	ob_start();
	session_start();
	if(!empty($_SESSION['user_name'])){
		return true;
	}else{
		exit(header('Location: login.php'));
	}
	 ob_end_flush(); 
}

function getPharmacy()
{
	echo json_encode($_GET);
}

?>