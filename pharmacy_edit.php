<?php include 'layout/header.php'; ?>

 <?php 

 
  $sql_category = "SELECT * FROM category";
  $category = $conn->query($sql_category);

  $sql_unit = "SELECT * FROM unit";
  $unit = $conn->query($sql_unit);

 if(!empty($_GET['id'])){
  $id =  $_GET['id']; 
    $sql = "SELECT * FROM `pharmacy` WHERE pharmacy_id = $id";
  $result = $conn->query($sql);
  $pharmacy = mysqli_fetch_assoc($result);
 }
  
  if ( !empty($_POST)) {
        
        $name = $_POST['pharmacy_name'];
        $quantity = $_POST['pharmacy_quantity'];
        $date = $_POST['expired_date'];
        $category = $_POST['pharmacy_category'];
        $manufacture = $_POST['pharmacy_manufacture'];
        $unit = $_POST['pharmacy_unit'];
        $buy = $_POST['buy_price'];
        $sell = $_POST['sell_price'];
        $id = $_POST['pharmacy_id'];
        // validate input
        $valid = true;
        if (empty($name)) {
            $valid = false;
        }
        if (empty($date)) {
            $valid = false;
        }
        if (empty($category)) {
            $valid = false;
        }
        if (empty($manufacture)) {
            $valid = false;
        }
        if (empty($unit)) {
            $valid = false;
        }
        if (empty($buy)) {
            $valid = false;
        }
        if (empty($sell)) {
            $valid = false;
        }
        if (empty($quantity)) {
            $valid = false;
        }
        // insert data
        if ($valid) {
            
            $sql = "UPDATE `pharmacy` SET pharmacy_name='$name',expired_date='$date',pharmacy_category='$category',pharmacy_manufacture='$manufacture',pharmacy_unit='$unit',pharmacy_quantity='$quantity',buy_price='$buy',sell_price='$sell' WHERE pharmacy_id=$id";
            $res = mysqli_query($conn, $sql);
            
            if($res){
              exit(header('Location: pharmacy_list.php'));
            }else{
              $fmsg = "Data not inserted, please try again later.";
            }
          
        }
    }

 ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
         <div class="page-title">
              <div class="title_left">
                <h3>Pharmacy </h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
             <div class="x_panel">
                  <div class="x_title">
                    <h2>Pharmacy Form <small>Pharmacy Edit</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <form id="demo-form2" action="pharmacy_edit.php" method="post" data-parsley-validate class="form-horizontal form-label-left">
                    <?php if(!empty($fmsg)){ echo $fmsg;} ?>
                      <div class="form-group">
                      <input type="hidden" name="pharmacy_id" value="<?php if(!empty($id)){ echo $id; } ?>" >
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Pharmacy Name <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="pharmacy_name" name="pharmacy_name" required="required" value="<?php if(!empty($pharmacy['pharmacy_name'])){ echo $pharmacy['pharmacy_name']; } ?>" class="form-control col-md-7 col-xs-12">
                        </div>

                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Expired Date <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="datepicker" name="expired_date" required="required" value="<?php if(!empty($pharmacy['expired_date'])){ echo $pharmacy['expired_date']; } ?>" class="form-control col-md-7 col-xs-12 datepicker">
                        </div>

                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Category<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select name="pharmacy_category" class="form-control" required="required">
                          <option value="">Select Category</option>
                          <?php while($cat = $category->fetch_assoc()) { ?>
                            <?php if($pharmacy['pharmacy_category'] == $cat['category_id']){ ?>
                              <option value="<?php echo $cat['category_id']; ?>" selected="selected"><?php echo $cat['category_name']; ?></option>
                            <?php }else{ ?>
                               <option value="<?php echo $cat['category_id']; ?>"><?php echo $cat['category_name']; ?></option>
                            <?php } ?>
                          
                          <?php }?>
                          </select>
                        </div>

                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Manufacture<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="pharmacy_manufacture" name="pharmacy_manufacture" required="required" class="form-control col-md-7 col-xs-12" value="<?php if(!empty($pharmacy['pharmacy_manufacture'])){ echo $pharmacy['pharmacy_manufacture']; } ?>">
                        </div>

                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Unit<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                         <select name="pharmacy_unit" class="form-control" required="required">
                          <option value="">Select Unit</option>
                          <?php while($u = $unit->fetch_assoc()) { ?>
                          <?php if($pharmacy['pharmacy_unit'] == $u['unit_id']){ ?>
                          <option value="<?php echo $u['unit_id']; ?>" selected="selected"><?php echo $u['unit_name']; ?></option>
                          <?php }else{ ?>
                          <option value="<?php echo $u['unit_id']; ?>"><?php echo $u['unit_name']; ?></option>
                            <?php } ?>
                          <?php }?>
                          </select>
                        </div>

                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Quantity<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="pharmacy_quantity" name="pharmacy_quantity" required="required" value="<?php if(!empty($pharmacy['pharmacy_quantity'])){ echo $pharmacy['pharmacy_quantity']; } ?>" class="form-control col-md-7 col-xs-12">
                        </div>

                      </div>
                     <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Buy Price<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="buy_price" name="buy_price" required="required" class="form-control col-md-7 col-xs-12" value="<?php if(!empty($pharmacy['buy_price'])){ echo $pharmacy['buy_price']; } ?>">
                        </div>

                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Sell Price<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="sell_price" name="sell_price" required="required" class="form-control col-md-7 col-xs-12" value="<?php if(!empty($pharmacy['sell_price'])){ echo $pharmacy['sell_price']; } ?>">
                        </div>

                      </div>
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          <a href="pharmacy_list.php" class="btn btn-primary" >Cancel</a>
                          <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
            </div>

          </div>
          <br />

          </div>
        

    <?php include 'layout/footer.php'; ?>
  
