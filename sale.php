
<?php include 'layout/header.php'; ?>
 <?php 
  if ( !empty($_POST)) {
         
        // keep track post values
        $pharmacy = $_POST['pharmacy'];
        $pharmacy_qty = $_POST['pharmacy_qty'];
        $price = $_POST['price'];
        $supplier = $_POST['supplier'];
        $sale_date = date('Y-m-d');
        // validate input
        $valid = true;
        if (empty($pharmacy)) {
            $valid = false;
        }
        if (empty($pharmacy_qty)) {
            $valid = false;
        }
        if (empty($price)) {
            $valid = false;
        }
        if (empty($supplier)) {
            $valid = false;
        }
        $todate = date('Y-m-d');
        // insert data
        if ($valid) {
          
          $check = "SELECT * FROM pharmacy WHERE pharmacy_id = '$pharmacy' AND  pharmacy_quantity >= '0' AND  expired_date > '$todate'";
            
          if($result = mysqli_query($conn,$check)){
              $total =  mysqli_num_rows($result);
              
            if($total > 0){
              $sql = "INSERT INTO sale (pharmacy,qty,sale_date,price,supplier) values('$pharmacy','$pharmacy_qty','$sale_date','$price','$supplier')";
              $res = $conn->query($sql);
            
              if($res){
                $sql_qty = "UPDATE `pharmacy` SET pharmacy_quantity= pharmacy_quantity - '$pharmacy_qty' WHERE pharmacy_id='$pharmacy'";
              $res_qty = $conn->query($sql_qty);
             
                exit(header('Location: sale_list.php'));
              }else{
                $fmsg = "Data not inserted, please try again later.";
              }
            }else{
              $fmsg = "Please check this pharmacy item not quantity or expired";
            }
             
          }
            
          
        }
    }
    $sql_category = "SELECT * FROM category";
    $category = $conn->query($sql_category);

    $sql_pharmacy = "SELECT * FROM pharmacy";
    $pharmacy = $conn->query($sql_pharmacy);

    $sql_supplier = "SELECT * FROM supplier";
    $supplier = $conn->query($sql_supplier);
 ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
         <div class="page-title">
              <div class="title_left">
                <h3>Sale</h3>
              </div>

              <div class="title_right">
                <div class="col-md-6 col-sm-6 col-xs-12 form-group pull-right top_search">
                  
                    <?php if(!empty($fmsg)){ ?>
                      <div class="alert alert-danger">
                     <?php echo $fmsg; ?>
                        </div>
                     <?php }?>
                
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
             <div class="x_panel">
                  <div class="x_title">
                    <h2>Sale Form <small>Pharmacy list</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <form id="demo-form2" action="sale.php" method="post" data-parsley-validate class="form-label-left">
                      <div class="form-group col-md-3">
                        <label class="control-label" for="first-name">Category Name <span class="required">*</span>
                        </label>
                        <div class="">
                           <select name="category" class="form-control" required="required" id="category">
                          <option value="">Select Category</option>
                          <?php while($cat = $category->fetch_assoc()) { ?>
                          <option value="<?php echo $cat['category_id']; ?>"><?php echo $cat['category_name']; ?></option>
                          <?php }?>
                          </select>
                        </div>

                      </div>
                      <div class="form-group col-md-3">
                        <label class="control-label" for="first-name">Pharmacy Name <span class="required">*</span>
                        </label>
                        <div class="">
                           <select name="pharmacy" class="form-control" required="required" id="pharmacy">
                          <option value="">Select Pharmacy</option>
                          
                          </select>
                        </div>

                      </div>
                      <div class="form-group col-md-1">
                        <label class="control-label" for="first-name">Quantity <span class="required">*</span>
                        </label>
                        <div class="">
                          <input type="number" id="pharmacy_qty" name="pharmacy_qty" required="required" class="form-control" value="1">
                        </div>

                      </div>
                      <div class="form-group col-md-2">
                        <label class="control-label" for="first-name">Price<span class="required">*</span>
                        </label>
                        <div class="">
                          <input type="text" id="price" name="price" required="required" class="form-control" readonly="readonly">
                        </div>

                      </div>
                      
                      <div class="form-group col-md-3">
                        <label class="control-label" for="first-name">Customer<span class="required">*</span>
                        </label>
                        <div class="">
                         <select name="supplier" class="form-control" required="required" id="supplier">
                          <option value="">Select Customer</option>
                          <?php while($u = $supplier->fetch_assoc()) { ?>
                          <option value="<?php echo $u['supplier_id']; ?>"><?php echo $u['supplier_name']; ?></option>
                          <?php }?>
                          </select>
                        </div>

                      </div>

                      <div class="form-group col-md-12">
                      <label class="control-label" for="first-name"><span class="required"></span>
                        </label>
                        <div class="">
                          <a href="sale_list.php" class="btn btn-primary" >Cancel</a>
                          <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
            </div>

          </div>
          <br />

          </div>
        

    <?php include 'layout/footer.php'; ?>
