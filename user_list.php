
<?php include 'layout/header.php'; ?>
 <?php 

  $sql = "SELECT * FROM users";
  $result = $conn->query($sql);
  if(!empty($_GET['id'])){
  $id =  $_GET['id']; 
    $sql = "DELETE FROM `users` WHERE user_id=$id";
  $delete = $conn->query($sql);
  if($delete){
    $message = "Delete Success";
  }
 }
 ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
         <div class="page-title">
              <div class="title_left">
                <h3>User List</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <?php if(!empty($message)) { ?>
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <p class="alert alert-success"><?php echo $message; ?></p>
                  <?php } ?>
                      <a href="user_form.php" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Add New</a>
                  
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>User List <small>Users</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                   
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>Name</th>
                          <th>Edit</th>
                          <th>Delete</th>
                          
                        </tr>
                      </thead>


                      <tbody>
                        <?php while($row = $result->fetch_assoc()) { ?>
                          <tr>
                            <td><?php echo $row['user_name']; ?></td>
                            <td><a href="user_edit.php?id=<?php echo $row['user_id']; ?>" class="btn btn-primary"><i class="fa fa-edit"></i> Edit</a></td>
                            <td><a href="user_list.php?id=<?php echo $row['user_id']; ?>" class="btn btn-danger"><i class="fa fa-remove"></i> Delete</a></td>
                          </tr>
                   <?php }?>
                      </tbody>
                    </table>
                  </div>
                </div>
            </div>

          </div>
          <br />

          </div>
        

    <?php include 'layout/footer.php'; ?>
	
  
