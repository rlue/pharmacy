<?php include 'layout/header.php'; ?>

 <?php 

 
  $sql_category = "SELECT * FROM category";
  $category = $conn->query($sql_category);

  $sql_unit = "SELECT * FROM unit";
  $unit = $conn->query($sql_unit);

 if(!empty($_GET['id'])){
  $id =  $_GET['id']; 
    $sql = "SELECT * FROM `pharmacy` LEFT JOIN category
ON pharmacy.pharmacy_category=category.category_id LEFT JOIN unit
ON pharmacy.pharmacy_unit=unit.unit_id WHERE pharmacy_id = $id";
  $result = $conn->query($sql);
  $pharmacy = mysqli_fetch_assoc($result);
 }
  
 
 ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
         <div class="page-title">
              <div class="title_left">
                <h3>Pharmacy </h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
             <div class="x_panel">
                  <div class="x_title">
                    <h2>Pharmacy Detail <small>Pharmacy Detail</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                   <table class="table table-bordered">
                     <tr>
                       <th>#</th>
                       <th>#</th>
                     </tr>
                     <tr>
                       <td><strong>Name</strong></td>
                       <td><?php echo $pharmacy['pharmacy_name']; ?></td>
                     </tr>
                     <tr>
                       <td><strong>Expired</strong></td>
                       <td><?php echo $pharmacy['expired_date']; ?></td>
                     </tr>
                     <tr>
                       <td><strong>Category</strong></td>
                       <td><?php echo $pharmacy['category_name']; ?></td>
                     </tr>
                     <tr>
                       <td><strong>Manufacture</strong></td>
                       <td><?php echo $pharmacy['pharmacy_manufacture']; ?></td>
                     </tr>
                     <tr>
                       <td><strong>Unit</strong></td>
                       <td><?php echo $pharmacy['unit_name']; ?></td>
                     </tr>
                     <tr>
                       <td><strong>Quantity</strong>/td>
                       <td><?php echo $pharmacy['pharmacy_quantity']; ?></td>
                     </tr>
                     <tr>
                       <td><strong>Sell</strong></td>
                       <td><?php echo $pharmacy['sell_price']; ?></td>
                     </tr>
                     <tr>
                       <td><strong>Buy</strong></td>
                       <td><?php echo $pharmacy['buy_price']; ?></td>
                     </tr>
                   </table>
                   <a href="pharmacy_list.php" class="btn btn-primary">Back</a>
                  </div>
                </div>
            </div>

          </div>
          <br />

          </div>
        

    <?php include 'layout/footer.php'; ?>
  
