
<?php include 'layout/header.php'; ?>
 <?php 
 if ( !empty($_POST)){
        $from = $_POST['fromDate'];
        $to = $_POST['toDate'];
        // validate input
        $valid = true;
        if (empty($from)) {
            $valid = false;
        }
        if (empty($to)) {
            $valid = false;
        }

    $sql = "SELECT * FROM sale LEFT JOIN pharmacy
ON sale.pharmacy=pharmacy.pharmacy_id LEFT JOIN supplier
ON sale.supplier=supplier.supplier_id WHERE sale_date >= '$from' AND  sale_date <= '$to'";
  $result = $conn->query($sql);
  
 }else{
    $sql = "SELECT * FROM sale LEFT JOIN pharmacy
ON sale.pharmacy=pharmacy.pharmacy_id LEFT JOIN supplier
ON sale.supplier=supplier.supplier_id";
  $result = $conn->query($sql);
 }
  
  if(!empty($_GET['id'])){
  $id =  $_GET['id']; 
    $sql = "DELETE FROM `sale` WHERE sale_id=$id";
  $delete = $conn->query($sql);
  if($delete){
    header( "Location: sale_list.php" );
    $message = "Delete Success";
  }
 }
 ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
         <div class="page-title">
              <div class="title_left">
                <h3>Sale List</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <?php if(!empty($message)) { ?>
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <p class="alert alert-success"><?php echo $message; ?></p>
                  <?php } ?>
                      <a href="sale.php" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Add New</a>
                  
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Sale List <small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                   <div class="row">
                   <form action="sale_list.php" method="POST">
                   <div class="col-md-2"></div>
                    <div class="col-md-3"> 
                      <div class='input-group date' id='arrival_Date'>
                            <input type="text" id="fromDate" class='form-control' name="fromDate" placeholder="From Date" value="<?php if(!empty($_POST['fromDate'])){ echo $_POST['fromDate']; } ?>" required="required">
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                         </div>
                    </div>
                    
                    <div class="col-md-3">
                       <div class='input-group date' id='arrival_Date'>
                            <input type="text" id="toDate" class='form-control' name="toDate" placeholder="To Date" value="<?php if(!empty($_POST['toDate'])){ echo $_POST['toDate']; } ?>" required="required" >
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                         </div>
                    </div>
                     <div class="col-md-4">
                       <button type="submit" class="btn btn-primary">Search</button>
                     </div> 
                    
                  </div>
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>Name</th>
                          <th>Date</th>
                          <th>Buyer</th>
                          <th>Quantity</th>
                          <th>Price</th>
                          <th>Delete</th>
                          
                        </tr>
                      </thead>


                      <tbody>
                        <?php while($row = $result->fetch_assoc()) { ?>
                          <tr>
                            <td><?php echo $row['pharmacy_name']; ?></td>
                            <td><?php echo $row['sale_date']; ?></td>
                            <td><?php echo $row['supplier_name']; ?></td>
                            <td><?php echo $row['qty']; ?></td>
                            <td><?php echo $row['price']; ?></td>
                            <td><a href="sale_list.php?id=<?php echo $row['sale_id']; ?>" class="btn btn-danger"><i class="fa fa-remove"></i> Delete</a></td>
                            
                          </tr>
                   <?php }?>
                      </tbody>
                    </table>
                  </div>
                </div>
            </div>

          </div>
          <br />

          </div>
        

    <?php include 'layout/footer.php'; ?>
	
  
